package main

import "fmt"

func GetPidFromVisitno(visitno int64) int64 {
	pid := int64(0)
	db, err := GetMySQL()
	if err != nil {
		return pid
	}
	defer db.Close()

	st, err := db.Prepare("select `pid` from `visit` where `visitno`=?;")
	if err != nil {
		return pid
	}

	rows, err := st.Query(visitno)
	if err != nil {
		return pid
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&pid)
		return pid
	}

	return pid
}

func GetAllVisitnoFromPid(pid int64) []int64 {
	visitno := make([]int64, 0)
	db, err := GetMySQL()
	if err != nil {
		return visitno
	}
	defer db.Close()

	st, err := db.Prepare("select `visitno` from `visit` where `pid`=? order by `visitno` desc limit 30;")
	if err != nil {
		return visitno
	}

	rows, err := st.Query(pid)
	if err != nil {
		return visitno
	}
	defer rows.Close()

	for rows.Next() {
		visit := int64(0)
		rows.Scan(&visit)
		visitno = append(visitno, visit)
	}

	return visitno
}

func GetAllAppointFromDatePIDinTable(today int, field string, table string, typeapp string) []appointData {
	rdata := make([]appointData, 0)
	db, err := GetMySQL()
	if err != nil {
		return rdata
	}
	defer db.Close()

	st, err := db.Prepare("select `pid`, `" + field + "` from `" + table + "` where `" + field + "`>=?;")
	if err != nil {
		return rdata
	}

	year, month, day := ExtractDateFromInt(today)
	rows, err := st.Query(fmt.Sprintf("%d-%02d-%02d", year, month, day))
	if err != nil {
		return rdata
	}
	defer rows.Close()

	for rows.Next() {
		aData := appointData{}
		pid := int64(0)
		dateApp := ""
		rows.Scan(&pid, &dateApp)
		aData.visitno = 0
		aData.pid = pid
		aData.dateApp = ConvDateToInt(dateApp)
		aData.typeApp = typeapp
		rdata = append(rdata, aData)
	}
	return rdata

}

func GetAllAppointFromDateVisitnoinTable(today int, field string, table string, typeapp string) []appointData {
	rdata := make([]appointData, 0)
	db, err := GetMySQL()
	if err != nil {
		return rdata
	}
	defer db.Close()

	st, err := db.Prepare("select `visitno`, `" + field + "` from `" + table + "` where `" + field + "`>=?;")
	if err != nil {
		return rdata
	}

	year, month, day := ExtractDateFromInt(today)
	rows, err := st.Query(fmt.Sprintf("%d-%02d-%02d", year, month, day))
	if err != nil {
		return rdata
	}
	defer rows.Close()

	for rows.Next() {
		aData := appointData{}
		visitno := int64(0)
		dateApp := ""
		rows.Scan(&visitno, &dateApp)
		aData.visitno = visitno
		aData.dateApp = ConvDateToInt(dateApp)
		aData.pid = 0
		aData.typeApp = typeapp
		rdata = append(rdata, aData)
	}

	return rdata
}

func ChkIDcardExist(idcard string) bool {
	count := 0
	db, err := GetMySQL()
	if err != nil {
		return false
	}
	defer db.Close()

	st, err := db.Prepare("select count(*) from `person` where `idcard`=?;")
	if err != nil {
		return false
	}

	rows, err := st.Query(idcard)
	if err != nil {
		return false
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&count)
	}
	if count == 1 {
		return true
	}
	return false
}

func GetIDcardFromPid(pid int64) string {
	idcard := ""

	db, err := GetMySQL()
	if err != nil {
		return ""
	}
	defer db.Close()

	st, err := db.Prepare("select `idcard` from `person` where `pid`=?;")
	if err != nil {
		return ""
	}

	rows, err := st.Query(pid)
	if err != nil {
		return ""
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&idcard)
		return idcard
	}

	return ""
}

func GetBirthDayFromIDcard(idcard string) int {
	day := 0
	month := 0
	year := 0
	birth := 0

	db, err := GetMySQL()
	if err != nil {
		return 0
	}
	defer db.Close()

	st, err := db.Prepare("select day(`birth`), month(`birth`), year(`birth`) from `person` where `idcard`=?;")
	if err != nil {
		return 0
	}

	rows, err := st.Query(idcard)
	if err != nil {
		return 0
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&day, &month, &year)
		birth = (year * 10000) + (month * 100) + day
		return birth
	}

	return 0
}

func GetPersonNameFromPID(pid int64) (string, string) {
	fname := ""
	lname := ""
	db, err := GetMySQL()
	if err != nil {
		return "", ""
	}
	defer db.Close()

	st, err := db.Prepare("select `fname`,`lname` from `person` where `pid`=?;")
	if err != nil {
		return "", ""
	}

	rows, err := st.Query(pid)
	if err != nil {
		return "", ""
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&fname, &lname)
	}
	return fname, lname
}

func GetPersonFromIDcard(idcard string) (int64, string, string) {
	pid := int64(0)
	fname := ""
	lname := ""
	db, err := GetMySQL()
	if err != nil {
		return 0, "", ""
	}
	defer db.Close()

	st, err := db.Prepare("select `pid`,`fname`,`lname` from `person` where `idcard`=?;")
	if err != nil {
		return 0, "", ""
	}

	rows, err := st.Query(idcard)
	if err != nil {
		return 0, "", ""
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&pid, &fname, &lname)
	}
	return pid, fname, lname
}

func InsertUserHealth(uid int64, pid int64) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("insert into `userHealth` (`uid`, `pid`) values (?,?);")
	_, _ = st.Exec(uid, pid)
}

func DeleteUserHealth(uid int64) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `userHealth` where `uid`=?;")
	_, _ = st.Exec(uid)
}

func GetAllUserHealth() (user map[int64]int64) {
	user = make(map[int64]int64)
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `uid`,`pid` from `userHealth`;")
	rows, err := st.Query()
	if err != nil {
		return
	}
	defer rows.Close()

	uid := int64(0)
	pid := int64(0)

	for rows.Next() {
		rows.Scan(&uid, &pid)
		user[uid] = pid
	}
	return
}

func SaveAdviceUser(appType string, appText string) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `adviceUser` where `appType`=?;")
	_, _ = st.Exec(appType)

	st, _ = con.Prepare("insert into `adviceUser` (`appType`, `appText`) values (?,?);")
	_, _ = st.Exec(appType, appText)
}

func DeleteAdviceUser(appType string) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `adviceUser` where `appType`=?;")
	_, _ = st.Exec(appType)
}

func GetAdviceUser(appType string) string {
	appText := ""
	db, err := GetSQLite()
	if err != nil {
		return ""
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `appText` from `adviceUser` where appType=?;")
	rows, err := st.Query(appType)
	if err != nil {
		return ""
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&appText)
		return appText
	}
	return ""
}

func SaveAppointChangeWithDeleteOld(pid int64, appType string, dateApp int, dateApp2 int) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `appointChange` where `pid`=? and `typeApp`=? and `dateApp`=?;")
	_, _ = st.Exec(pid, appType, dateApp)

	st, _ = con.Prepare("insert into `appointChange` (`pid`, `typeApp`, `dateApp`, `dateApp2`) values (?,?,?,?);")
	_, _ = st.Exec(pid, appType, dateApp, dateApp2)
}

func GetAllAppointChange() []appointChange {
	appChange := make([]appointChange, 0)
	db, err := GetSQLite()
	if err != nil {
		return appChange
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `aid`, `pid`, `typeApp`, `dateApp`, `dateApp2` from `appointChange`;")
	rows, err := st.Query()
	if err != nil {
		return appChange
	}
	defer rows.Close()

	for rows.Next() {
		acc := appointChange{
			aid:      int64(0),
			pid:      int64(0),
			typeApp:  "",
			dateApp:  0,
			dateApp2: 0,
		}
		rows.Scan(&acc.aid, &acc.pid, &acc.typeApp, &acc.dateApp, &acc.dateApp2)
		appChange = append(appChange, acc)
	}

	return appChange
}

func DeleteAppointChangeFromAid(aid int64) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `appointChange` where `aid`=?;")
	_, _ = st.Exec(aid)
}

func LoadOrsormorDB() map[int64]string {
	orsormorOut := make(map[int64]string)
	db, err := GetSQLite()
	if err != nil {
		return orsormorOut
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `uid`, `name` from `orsormor`;")
	rows, err := st.Query()
	if err != nil {
		return orsormorOut
	}
	defer rows.Close()

	for rows.Next() {
		uid := int64(0)
		name := ""
		rows.Scan(&uid, &name)
		orsormorOut[uid] = name
	}

	return orsormorOut

}

func LoadOrsormorCareDB() map[int64][]int64 {
	orsormorOut := make(map[int64][]int64)
	orsormorUser := orsormorData.ListOrsormor()
	for i := range orsormorUser {
		orsormorOut[orsormorUser[i]] = make([]int64, 0)
	}
	db, err := GetSQLite()
	if err != nil {
		return orsormorOut
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `uid`, `pid` from `orsormorCare`;")
	rows, err := st.Query()
	if err != nil {
		return orsormorOut
	}
	defer rows.Close()

	for rows.Next() {
		uid := int64(0)
		pid := int64(0)
		rows.Scan(&uid, &pid)
		orsormorOut[uid] = append(orsormorOut[uid], pid)
	}

	return orsormorOut
}

func SaveOrsormorDB(uid int64, name string) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `orsormor` where `uid`=?;")
	_, _ = st.Exec(uid)

	st, _ = con.Prepare("insert into `orsormor` (`uid`, `name`) values (?,?);")
	_, _ = st.Exec(uid, name)
}

func DeleteOrsormorDB(uid int64) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `orsormor` where `uid`=?;")
	_, _ = st.Exec(uid)

	st, _ = con.Prepare("delete from `orsormorCare` where `uid`=?;")
	_, _ = st.Exec(uid)
}

func AddOrsormorCareDB(uid int64, pid int64) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("insert into `orsormorCare` (`uid`, `pid`) values (?,?);")
	_, _ = st.Exec(uid, pid)
}

func RemoveCareFromOrsormorDB(uid int64, pid int64) {
	db, err := GetSQLite()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("delete from `orsormorCare` where `uid`=? and `pid`=?;")
	_, _ = st.Exec(uid, pid)
}
