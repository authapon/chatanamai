module chatanamai

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/mattn/go-sqlite3 v1.14.12
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require github.com/technoweenie/multipartstreamer v1.0.1 // indirect
