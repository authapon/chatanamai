package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"database/sql"

	_ "github.com/mattn/go-sqlite3"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/yaml.v3"
)

type (
	configData struct {
		Token     string       `yaml:token`
		Token2    string       `yaml:token2`
		Operators []operatorST `yaml:operators`
		Mysql     string       `yaml:mysql`
		Sqlite    string       `yaml:sqlite`
		AlertTime string       `yaml:alerttime`
	}

	operatorST struct {
		Name string `yaml:name`
		ID   int64  `yaml:id`
	}
)

var (
	token       string
	token2      string
	operators   []operatorST
	mysqlServer string
	sqliteDB    string
	alertTime   string
)

func ConfigProcess() {
	if len(os.Args) > 1 {
		yfile, err := ioutil.ReadFile(os.Args[1])
		if err != nil {
			fmt.Printf("Cannot load file config\n")
			Usage()
		}

		configDat := configData{}
		err2 := yaml.Unmarshal(yfile, &configDat)
		if err2 != nil {
			fmt.Printf("Error in file config\n")
			Usage()
		}

		token = configDat.Token
		token2 = configDat.Token2
		operators = configDat.Operators
		mysqlServer = configDat.Mysql
		sqliteDB = configDat.Sqlite
		alertTime = configDat.AlertTime

		return
	}
	Usage()
}

func Usage() {
	fmt.Printf("Usage: chatanamai <config.yml>\n\n")
	fmt.Printf("Example config:\n-------------\n")
	fmt.Printf("token: \"111111:AAELxxusQJIoGkuCiJWtZnJAyIgYccvskwg\"\n")
	fmt.Printf("token2: \"111111:AAFxf-apcymrxjreZ4dbdotgRz8mspTuigQ\"\n")
	fmt.Printf("operators:\n")
	fmt.Printf("  - name: \"Oper1\"\n")
	fmt.Printf("    id : 1\n")
	fmt.Printf("mysql: \"root:passwd@tcp(127.0.0.1:3306)/jhcisdb\"\n")
	fmt.Printf("sqlite: \"chatanamai.db\"\n")
	fmt.Printf("alerttime: \"07:00\"\n")

	os.Exit(1)
}

func GetMySQL() (*sql.DB, error) {
	con, err := sql.Open("mysql", mysqlServer)
	if err != nil {
		return con, err
	}
	err = con.Ping()
	if err != nil {
		return con, err
	}

	return con, nil
}

func GetSQLite() (*sql.DB, error) {
	con, err := sql.Open("sqlite3", sqliteDB)
	if err != nil {
		return con, err
	}
	err = con.Ping()
	if err != nil {
		return con, err
	}

	return con, nil
}
