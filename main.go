package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	//tg "gopkg.in/telegram-bot-api.v5"
	tg "github.com/go-telegram-bot-api/telegram-bot-api"
)

type (
	appointChange struct {
		aid      int64
		pid      int64
		typeApp  string
		dateApp  int
		dateApp2 int
	}
)

var (
	bot           *tg.BotAPI
	mutexSendBot  = &sync.Mutex{}
	bot2          *tg.BotAPI
	mutexSendBot2 = &sync.Mutex{}
	user          = &userHealthMap{
		pid:   make(map[int64]int64),
		state: make(map[int64]map[string]string),
	}
	oper       = make(map[int64]string)
	appointVar = &appointMatrix{
		appoint: make([]appointData, 0),
	}
	orsormorData = &orsormor{
		uid:       make(map[int64]string),
		care:      make(map[int64][]int64),
		careFName: make(map[int64]string),
		careLName: make(map[int64]string),
		state:     make(map[int64]map[string]string),
	}
)

func StartBOT() {
	var err error
	for {
		bot, err = tg.NewBotAPI(token)
		if err != nil {
			fmt.Printf("Trying to connect telegram again...(%v)\n", token)
			time.Sleep(5 * time.Second)
			continue
		}
		break
	}
	u := tg.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		fmt.Printf("Error to get channel for bot\n")
		os.Exit(1)
	}

	for update := range updates {
		go func(u tg.Update) {
			switch u.Message {
			case nil:
				return
			default:
				uid := int64(u.Message.From.ID)

				if u.Message.Text == "" {
					return
				}

				text := strings.TrimSpace(u.Message.Text)

				switch {
				case user.Get(uid) > 0:
					if user.GetState(uid, "fname") == "" {
						GetUserData(uid)
					}
				}

				switch text[0] {
				case '/':
					user.SetState(uid, "state", "")
					orsormorData.SetState(uid, "state", "")
				}

				switch text {
				case "/start":
					go SendMsg(uid, "ยินต้อนรับสู่การใช้งานบอทการแจ้งการนัดหมายแพทย์อนามัย")
				case "/chklogin", "/logout", "/appointment", "/change", "/talk":
					switch orsormorData.IsOrsormor(uid) {
					case false:
						switch user.Get(uid) {
						case 0:
							go SendMsg(uid, "ท่านยังไม่ได้ทำการลงทะเบียน")
							return
						}
					}
				}

				switch text {
				case "/start":
					return
				case "/login":
					go func() {
						switch orsormorData.IsOrsormor(uid) {
						case true:
							SendMsg(uid, "กรุณาระบุรหัสประจำตัวประชาชนผู้รับบริการในการดูแลของท่าน")
							orsormorData.SetState(uid, "state", "login")
						default:
							switch user.Get(uid) {
							case 0:
								user.SetState(uid, "state", "login:id")
								SendMsg(uid, "กรุณาระบุรหัสประจำตัวประชาชน")
							default:
								SendMsg(uid, "ท่านได้ลงทะเบียนไว้แล้ว")
								SendMsg(uid, fmt.Sprintf("ท่านได้ลงทะเบียนไว้แล้ว\nคุณ %s %s\n", user.GetState(uid, "fname"), user.GetState(uid, "lname")))
							}
						}
					}()
				case "/chklogin":
					go func() {
						switch orsormorData.IsOrsormor(uid) {
						case true:
							pid, fname, lname := orsormorData.GetOrsormorAllCare(uid)
							switch len(pid) {
							case 0:
								SendMsg(uid, "ท่านยังไม่มีผู้รับบริการในการดูแล")
							default:
								dtext := "รายนามผู้รับบริการในการดูแล\n\n"
								for i := range pid {
									dtext += fmt.Sprintf("คุณ %s %s (%d)\n", fname[i], lname[i], pid[i])
								}
								SendMsg(uid, dtext)
							}
						default:
							SendMsg(uid, fmt.Sprintf("ท่านได้ลงทะเบียนไว้แล้ว\nคุณ %s %s\n", user.GetState(uid, "fname"), user.GetState(uid, "lname")))
						}
					}()
				case "/logout":
					go func() {
						switch orsormorData.IsOrsormor(uid) {
						case true:
							SendMsg(uid, "กรุณาป้อนรหัสหมายเลขผู้รับริการในการดูแล")
							orsormorData.SetState(uid, "state", "logout")
						default:
							fname := user.GetState(uid, "fname")
							lname := user.GetState(uid, "lname")
							DeleteUserHealth(uid)
							user.Set(uid, 0)
							SendMsg(uid, fmt.Sprintf("ได้ทำการถอนการลงทะเบียนเรียบร้อยแล้ว\nคุณ %s %s", fname, lname))
						}
					}()
				case "/appointment":
					go func() {
						switch orsormorData.IsOrsormor(uid) {
						case true:
							AlertAppointmentOrsormor(uid, 0)
						default:
							AlertAppointment(uid, 0)
						}
					}()
				case "/change":
					go func() {
						switch orsormorData.IsOrsormor(uid) {
						case true:
							pid, fname, lname := orsormorData.GetOrsormorAllCare(uid)
							appx := false
							for ix := range pid {
								appoint := GetAppointment(pid[ix])
								switch len(appoint) {
								case 0:
								default:
									appx = true
									for i := range appoint {
										ii := i + 1
										switch appoint[i].typeApp {
										case "Diag":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดพบแพทย์เพื่อตรวจ_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										case "EPI":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับวัคซีน_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										case "FP":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการวางแผนครอบครัว_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										case "Home":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดเพื่อตรวจเยี่ยมบ้าน_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										case "Nutri":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดเพื่อตรวจโภชนาการ_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										case "Baby":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการการดูแลทารกหลังคลอด_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										case "Mother":
											SendMsg(uid, fmt.Sprintf("%d.%d. *คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการการดูแลหลังคลอดมารดา_\n\nใน %s", pid[ix], ii, fname[ix], lname[ix], pid[ix], ShowDateText(appoint[i].dateApp)))
										}
										orsormorData.SetState(uid, "change:appType:"+fmt.Sprintf("%d.%d", pid[ix], ii), appoint[i].typeApp)
										orsormorData.SetState(uid, "change:dateApp:"+fmt.Sprintf("%d.%d", pid[ix], ii), fmt.Sprintf("%d", appoint[i].dateApp))
									}
									user.SetState(uid, "change:choice:max:"+fmt.Sprintf("%d", pid[ix]), fmt.Sprintf("%d", len(appoint)))
								}
							}
							if !appx {
								SendMsg(uid, "ไม่มีการนัดหมายใดใด")
								return
							}
							SendMsg(uid, "ท่านต้องการเปลี่ยนวันนัดใหม่ กรุณาระบุหมายเลขการนัด หรือไม่ต้องการเปลี่ยน กรุณาป้อน 0")
							orsormorData.SetState(uid, "state", "change:choice")
						default:
							appoint := GetAppointment(user.Get(uid))
							switch len(appoint) {
							case 0:
								SendMsg(uid, "ท่านไม่มีนัดพบแพทย์")
							default:
								for i := range appoint {
									ii := i + 1
									switch appoint[i].typeApp {
									case "Diag":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดพบแพทย์เพื่อตรวจ\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									case "EPI":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดเพื่อรับวัคซีน\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									case "FP":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดเพื่อรับบริการวางแผนครอบครัว\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									case "Home":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดเพื่อตรวจเยี่ยมบ้าน\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									case "Nutri":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดเพื่อตรวจโภชนาการ\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									case "Baby":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดเพื่อรับบริการการดูแลทารกหลังคลอด\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									case "Mother":
										SendMsg(uid, fmt.Sprintf("%d. ท่านมีนัดเพื่อรับบริการการดูแลหลังคลอดมารดา\nใน %s", ii, ShowDateText(appoint[i].dateApp)))
									}
									user.SetState(uid, "change:appType:"+fmt.Sprintf("%d", ii), appoint[i].typeApp)
									user.SetState(uid, "change:dateApp:"+fmt.Sprintf("%d", ii), fmt.Sprintf("%d", appoint[i].dateApp))
								}
								SendMsg(uid, "ท่านต้องการเปลี่ยนวันนัดใหม่ กรุณาระบุหมายเลขการนัด หรือไม่ต้องการเปลี่ยน กรุณาป้อน 0")
								user.SetState(uid, "state", "change:choice")
								user.SetState(uid, "change:choice:max", fmt.Sprintf("%d", len(appoint)))

							}
						}
					}()
				case "/talk":
					go func() {
						SendMsg(uid, "กรุณป้อนข้อความที่จะส่งไปยังเจ้าหน้าที่")
						switch orsormorData.IsOrsormor(uid) {
						case true:
							orsormorData.SetState(uid, "state", "talk")
						default:
							user.SetState(uid, "state", "talk")
						}
					}()
				default:
					switch text[0] {
					case '/':
						go SendMsgNotUS(uid)
					default:
						switch orsormorData.IsOrsormor(uid) {
						case true:
							switch orsormorData.GetState(uid, "state") {
							case "login":
								go func() {
									orsormorData.SetState(uid, "state", "")
									switch ChkIDcardExist(text) {
									case true:
										pid, fname, lname := GetPersonFromIDcard(text)
										orsormorData.SetCareName(pid, fname, lname)
										orsormorData.AddOrsormorCare(uid, pid)
										SendMsg(uid, fmt.Sprintf("เพิ่มผู้รับบริการในการดูแลของท่านเรียบร้อยแล้ว\n\n*คุณ %s %s (%d)*", fname, lname, pid))
									default:
										SendMsg(uid, "ไม่สามารถเพิ่มผู้รับบริการได้")
									}
								}()
							case "logout":
								go func() {
									orsormorData.SetState(uid, "state", "")
									pid, err := strconv.ParseInt(text, 10, 64)
									if err != nil {
										SendMsg(uid, "ถอนการลงทะเบียนไม่สำเร็จ")
										return
									}
									switch orsormorData.RemoveCareFromOrsormor(uid, pid) {
									case true:
										fname, lname := orsormorData.GetCareName(pid)
										SendMsg(uid, fmt.Sprintf("ถอนการลงทะเบียนสำเร็จ\n\n*คุณ %s %s (%d)*", fname, lname, pid))
									default:
										SendMsg(uid, "ถอนการลงทะเบียนไม่สำเร็จ")
									}
								}()
							case "talk":
								go func() {
									orsormorData.SetState(uid, "state", "")
									name := orsormorData.GetOrsormorName(uid)
									AllTXT := fmt.Sprintf("*คุณ %s (x%d)*\n\n%s", name, uid, text)
									SendMsgAllOper(AllTXT)
								}()

							case "change:choice":
								go func() {
									tx := strings.Split(text, ".")
									switch len(tx) {
									case 2:
									default:
										orsormorData.SetState(uid, "state", "")
										SendMsg(uid, "ยกเลิกการเปลี่ยนวันนัดใหม่")
										return
									}
									choice, err := strconv.Atoi(tx[1])
									if err != nil {
										choice = 0
									}
									choiceMax, _ := strconv.Atoi(user.GetState(uid, "change:choice:max:"+tx[0]))
									if choice > choiceMax {
										choice = 0
									}
									switch choice {
									case 0:
										orsormorData.SetState(uid, "state", "")
										SendMsg(uid, "ยกเลิกการเปลี่ยนวันนัดใหม่")
									default:
										SendMsg(uid, "กรุณาป้อนวันนัดใหม่")
										orsormorData.SetState(uid, "state", "change:date")
										orsormorData.SetState(uid, "change:choice", text)
									}
								}()
							case "change:date":
								go func() {
									orsormorData.SetState(uid, "state", "")
									newdate := ConvDateSlashToInt(text)
									switch newdate {
									case 0:
										SendMsg(uid, "การป้อนข้อมูลไม่ถูกต้อง\nยกเลิกการเปลี่ยนวันนัดใหม่")
									default:
										choice := orsormorData.GetState(uid, "change:choice")
										ch := strings.Split(choice, ".")
										pidd, _ := strconv.ParseInt(ch[0], 10, 64)
										fname, lname := orsormorData.GetCareName(pidd)
										dateAppTXT := orsormorData.GetState(uid, "change:dateApp:"+choice)
										dateApp, _ := strconv.Atoi(dateAppTXT)
										txt := MakeTextForAppointChange(orsormorData.GetState(uid, "change:appType:"+choice))
										AllTXT := fmt.Sprintf("%s\n\nเปลี่ยนจาก %s\nไปเป็น %s", txt, ShowDateText(dateApp), ShowDateText(newdate))
										Rtext := fmt.Sprintf("*คุณ %s %s (%d)*\n\n", fname, lname, pidd) + AllTXT
										SendMsg(uid, Rtext)
										OperTXT := fmt.Sprintf("*คุณ %s %s (%d)*\nดำเนินการโดย คุณ %s (x%d)\n\n", fname, lname, pidd, orsormorData.GetOrsormorName(uid), uid) + AllTXT
										SendMsgAllOper(OperTXT)
										SaveAppointChangeWithDeleteOld(pidd, orsormorData.GetState(uid, "change:appType:"+choice), dateApp, newdate)
										SendMsg(uid, "ข้อมูลการเปลี่ยนวันนัดใหม่ ได้ส่งไปยังเจ้าหน้าที่แล้ว กรุณารอการตอบรับจากเจ้าหน้าที่ในเวลาต่อไปครับ")
									}
								}()
							default:
								go SendMsgNotUS(uid)
							}
						default:
							switch user.GetState(uid, "state") {
							case "login:id":
								go func() {
									user.SetState(uid, "idcard", text)
									switch ChkIDcardExist(text) {
									case true:
										user.SetState(uid, "state", "login:birthday")
										SendMsg(uid, "กรุณาระบุวันเดือนปีเกิด เช่น 15/01/2520 หมายถึง เกิดวันที่ 15 มกราคม 2520")
									default:
										SendMsg(uid, "ไม่พบรหัสในระบบ")
										SendMsg(uid, "ยกเลิกขั้นตอนการลงทะเบียน")
										user.SetState(uid, "state", "")
									}
								}()

							case "login:birthday":
								go func() {
									user.SetState(uid, "state", "")
									user.SetState(uid, "birthday", text)
									switch ChkUserBirthDay(uid) {
									case true:
										GetUserData(uid)
										InsertUserHealth(uid, user.Get(uid))
										SendMsg(uid, fmt.Sprintf("ยินดีต้อนรับ คุณ %s %s\n", user.GetState(uid, "fname"), user.GetState(uid, "lname")))
										SendMsg(uid, "ท่านได้ลงทะเบียนเรียบร้อยแล้วครับ")
									default:
										SendMsg(uid, "ข้อมูลที่ท่านป้อนไม่ตรงกับในระบบ")
										SendMsg(uid, "ยกเลิกขั้นตอนการลงทะเบียน")
									}
								}()
							case "change:choice":
								go func() {
									choice, err := strconv.Atoi(text)
									if err != nil {
										choice = 0
									}
									choiceMax, _ := strconv.Atoi(user.GetState(uid, "change:choice:max"))
									if choice > choiceMax {
										choice = 0
									}
									switch choice {
									case 0:
										user.SetState(uid, "state", "")
										SendMsg(uid, "ยกเลิกการเปลี่ยนวันนัดใหม่")
									default:
										SendMsg(uid, "กรุณาป้อนวันนัดใหม่")
										user.SetState(uid, "state", "change:date")
										user.SetState(uid, "change:choice", text)
									}
								}()
							case "change:date":
								go func() {
									user.SetState(uid, "state", "")
									newdate := ConvDateSlashToInt(text)
									switch newdate {
									case 0:
										SendMsg(uid, "การป้อนข้อมูลไม่ถูกต้อง\nยกเลิกการเปลี่ยนวันนัดใหม่")
									default:
										choice := user.GetState(uid, "change:choice")
										dateAppTXT := user.GetState(uid, "change:dateApp:"+choice)
										dateApp, _ := strconv.Atoi(dateAppTXT)
										txt := MakeTextForAppointChange(user.GetState(uid, "change:appType:"+choice))
										AllTXT := fmt.Sprintf("%s\n\nเปลี่ยนจาก %s\nไปเป็น %s", txt, ShowDateText(dateApp), ShowDateText(newdate))
										SendMsg(uid, AllTXT)
										OperTXT := fmt.Sprintf("*คุณ %s %s (%d)*\n\n", user.GetState(uid, "fname"), user.GetState(uid, "lname"), user.Get(uid)) + AllTXT
										SendMsgAllOper(OperTXT)
										SaveAppointChangeWithDeleteOld(user.Get(uid), user.GetState(uid, "change:appType:"+choice), dateApp, newdate)
										SendMsg(uid, "ข้อมูลการเปลี่ยนวันนัดใหม่ ได้ส่งไปยังเจ้าหน้าที่แล้ว กรุณารอการตอบรับจากเจ้าหน้าที่ในเวลาต่อไปครับ")
									}
								}()
							case "talk":
								go func() {
									user.SetState(uid, "state", "")
									AllTXT := fmt.Sprintf("*คุณ %s %s (%d)*\n\n%s", user.GetState(uid, "fname"), user.GetState(uid, "lname"), user.Get(uid), text)
									SendMsgAllOper(AllTXT)
								}()
							default:
								go SendMsgNotUS(uid)
							}
						}
					}
				}
			}
		}(update)
	}
}

func StartBOT2() {
	var err error
	for {
		bot2, err = tg.NewBotAPI(token2)
		if err != nil {
			fmt.Printf("Trying to connect telegram again...(%v)\n", token)
			time.Sleep(5 * time.Second)
			continue
		}
		break
	}
	u := tg.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot2.GetUpdatesChan(u)
	if err != nil {
		fmt.Printf("Error to get channel for bot\n")
		os.Exit(1)
	}

	for update := range updates {
		go func(u tg.Update) {
			switch u.Message {
			case nil:
				return
			default:
				uid := int64(u.Message.From.ID)
				if !IsOperators(uid) {
					go SendMsg2(uid, "ขออภัย ท่านไม่สามารถใช้งานบอทตัวนี้ได้ครับ")
					return
				}

				if u.Message.Text == "" {
					return
				}

				text := strings.TrimSpace(u.Message.Text)

				switch text[0] {
				case '/':
					oper[uid] = ""
				}

				switch text {
				case "/talk":
					go func() {
						SendMsg2(uid, "ป้อนรหัสผู้รับบริการและบรรทัดต่อไปป้อนข้อความครับ")
						oper[uid] = "talk"
					}()
				case "/inform":
					go func() {
						SendMsg2(uid, "กรุณาป้อนข้อความประชาสัมพันธ์")
						oper[uid] = "inform"
					}()
				case "/listapp":
					go func() {
						listApp := GetAllAppointChange()
						switch len(listApp) {
						case 0:
							SendMsg2(uid, "ไม่มีรายการขอเปลี่ยนการนัดหมาย")
						default:
							appCh := GetAllAppointChange()
							for i := range appCh {
								fname, lname := FindPersonNameFromPID(appCh[i].pid)
								SendMsg2(uid, fmt.Sprintf("*(%d) คุณ %s %s (%d)*\n\n%s\n\nเปลี่ยนจาก %s\nไปเป็น %s", appCh[i].aid, fname, lname, appCh[i].pid, MakeTextForAppointChange(appCh[i].typeApp), ShowDateText(appCh[i].dateApp), ShowDateText(appCh[i].dateApp2)))
							}
						}
					}()
				case "/informapp":
					go func() {
						SendMsg2(uid, "กรุณาระบบหมายเปลี่ยนนัด และการตอบรับในบรรทัดแรก และในบรรทัดที่สองเป็นคำอธิบายเพิ่มเติมหากมี เช่น 1 yes หรือ 15 no")
						oper[uid] = "informapp"
					}()
				case "/adviceset":
					go func() {
						SendMsg2(uid, "กรุณาระบุประเภทของการนัดในบรรทัดแรก และคำแนะนำในบรรทัดต่อมา ถ้าหากต้องการลบคำแนะนำให้พิมพ์แต่ประเภทของการนัดเพียงอย่างเดียว\n")
						SendMsg2(uid, "*ประเภทของการนัด*\nDiag - นัดพบแพทย์เพื่อตรวจ\nEPI - นัดเพื่อรับวัคซีน\nFP - นัดเพื่อรับบริการวางแผนครอบครัว\nHome - นัดเพื่อตรวจเยี่ยมบ้าน\nNutri - นัดเพื่อตรวจโภชนาการ\nBaby - นัดเพื่อรับบริการการดูแลทารกหลังคลอด\nMother - นัดเพื่อรับบริการการดูแลหลังคลอดมารดา")
					}()
					oper[uid] = "adviceset"
				case "/adviceshow":
					go func() {
						SendMsg2(uid, "กรุณาระบุประเภทการนัด")
						SendMsg2(uid, "*ประเภทของการนัด*\nDiag - นัดพบแพทย์เพื่อตรวจ\nEPI - นัดเพื่อรับวัคซีน\nFP - นัดเพื่อรับบริการวางแผนครอบครัว\nHome - นัดเพื่อตรวจเยี่ยมบ้าน\nNutri - นัดเพื่อตรวจโภชนาการ\nBaby - นัดเพื่อรับบริการการดูแลทารกหลังคลอด\nMother - นัดเพื่อรับบริการการดูแลหลังคลอดมารดา")
					}()
					oper[uid] = "adviceshow"
				case "/orsormorlist":
					go func() {
						orsor := orsormorData.GetOrsormorAllName()
						dataShow := ""
						for k, v := range orsor {
							dataShow += fmt.Sprintf("x%d - *%s*\n", k, v)
						}
						switch dataShow {
						case "":
							SendMsg2(uid, "ไม่มีข้อมูล อสม.")
						default:
							SendMsg2(uid, dataShow)
						}
					}()
				case "/orsormoradd":
					go func() {
						SendMsg2(uid, "กรุณาป้อนหมายเลข Telegram ID และบรรทัดต่อมาป้อนชื่อ")
						oper[uid] = "orsormoradd"
					}()
				case "/orsormordel":
					go func() {
						SendMsg2(uid, "กรุณาป้อนหมายเลข Telegram ID")
						oper[uid] = "orsormordel"
					}()
				case "/alertapp":
					go func() {
						SendMsg2(uid, "Alerting the Appointment")
						go AlertAppointment(0, 3)
						go AlertAppointmentOrsormor(0, 3)
					}()
				default:
					switch text[0] {
					case '/':
						go SendMsgNotUS2(uid)
					default:
						switch oper[uid] {
						case "talk":
							go func() {
								oper[uid] = ""
								talkX := strings.SplitAfterN(text, "\n", 2)
								switch len(talkX) {
								case 2:
									ppid := strings.TrimSpace(talkX[0])
									switch ppid[0] {
									case 'x', 'X':
										ppid = ppid[1:]
										pid, err := strconv.ParseInt(ppid, 10, 64)
										if err != nil {
											SendMsg2(uid, "มีข้อผิดพลาด")
											return
										}
										switch orsormorData.IsOrsormor(pid) {
										case true:
											SendMsgOrsormor(uid, pid, talkX[1])
										default:
											SendMsg2(uid, "มีข้อผิดพลาด")
										}
									default:
										pid, err := strconv.ParseInt(ppid, 10, 64)
										if err != nil {
											SendMsg2(uid, "มีข้อผิดพลาด")
											return
										}
										switch SendMsgPID(uid, pid, talkX[1]) {
										case true:
										default:
											SendMsg2(uid, "มีข้อผิดพลาด")
										}
									}
								default:
									SendMsg2(uid, "มีข้อผิดพลาด")
								}
							}()
						case "inform":
							go func() {
								oper[uid] = ""
								SendInform(uid, text)
							}()
						case "informapp":
							go func() {
								oper[uid] = ""
								inform := strings.SplitAfterN(text, "\n", 2)
								inform[0] = strings.TrimSpace(inform[0])
								informX := strings.Split(inform[0], " ")
								aid := int64(0)
								proceed := false
								cancel := false
								switch len(informX) {
								case 2:
									informX[0] = strings.TrimSpace(informX[0])
									informX[1] = strings.TrimSpace(informX[1])
									aid, err = strconv.ParseInt(informX[0], 10, 64)
									if err != nil {
										cancel = true
									}
									switch informX[1] {
									case "yes":
										proceed = true
									case "no":
										proceed = false
									default:
										cancel = true
									}
								default:
									cancel = true
								}
								if cancel {
									SendMsg2(uid, "มีข้อผิดพลาด ไม่มีการดำเนินการ")
									return
								}
								appCh := GetAllAppointChange()
								pid := int64(0)
								typeApp := ""
								dateApp := 0
								dateApp2 := 0
								cancel = true
								for i := range appCh {
									if appCh[i].aid == aid {
										pid = appCh[i].pid
										typeApp = appCh[i].typeApp
										dateApp = appCh[i].dateApp
										dateApp2 = appCh[i].dateApp2
										cancel = false
										break
									}
								}
								if cancel {
									SendMsg2(uid, "ไม่พบหมายเลขเลื่อนนัด ไม่มีการดำเนินการ")
									return
								}
								fname, lname := FindPersonNameFromPID(pid)
								appTXT := fmt.Sprintf("*การตอบรับการเลื่อนการนัดหมาย*\nคุณ %s %s (%d)\n\n%s\n\nจาก %s\nไปเป็น %s\n\n", fname, lname, pid, MakeTextForAppointChange(typeApp), ShowDateText(dateApp), ShowDateText(dateApp2))
								switch proceed {
								case true:
									appTXT += fmt.Sprintf("*ได้รับการอนุมัตแล้ว*\n\n")
								default:
									appTXT += fmt.Sprintf("*ไม่ได้รับการอนุมัต*\n\n")
								}
								if len(inform) > 1 {
									appTXT += inform[1]
								}
								SendMsgPID(uid, pid, appTXT)
								SendMsgAllOrsormorCare(uid, pid, appTXT)
								DeleteAppointChangeFromAid(aid)
							}()
						case "adviceset":
							go func() {
								oper[uid] = ""
								adviceData := strings.SplitAfterN(text, "\n", 2)
								adviceData[0] = strings.TrimSpace(adviceData[0])

								if !chkAppointCase(adviceData[0]) {
									SendMsg2(uid, "รหัสการนัดไม่ถูกต้อง")
									return
								}

								switch len(adviceData) {
								case 1:
									DeleteAdviceUser(adviceData[0])
									operName := GetOperNameFromUid(uid)
									SendMsgAllOper(fmt.Sprintf("_เจ้าหน้าที่ (%s) ลบคำแนะนำ_\n\n%s", operName, MakeTextForAppointAdvice(adviceData[0])))
								default:
									adviceData[1] = strings.TrimSpace(adviceData[1])
									SaveAdviceUser(adviceData[0], adviceData[1])
									operName := GetOperNameFromUid(uid)
									SendMsgAllOper(fmt.Sprintf("_เจ้าหน้าที่ (%s) ตั้งคำแนะนำ_\n\n%s\n%s", operName, MakeTextForAppointAdvice(adviceData[0]), adviceData[1]))
								}
							}()
						case "adviceshow":
							go func() {
								oper[uid] = ""
								if !chkAppointCase(text) {
									SendMsg2(uid, "รหัสการนัดไม่ถูกต้อง")
									return
								}
								adviceData := GetAdviceUser(text)
								SendMsg2(uid, fmt.Sprintf("%s\n\n%s", MakeTextForAppointAdvice(text), adviceData))
							}()
						case "orsormoradd":
							go func() {
								oper[uid] = ""
								data := strings.Split(text, "\n")
								switch len(data) {
								case 2:
									data[0] = strings.TrimSpace(data[0])
									data[1] = strings.TrimSpace(data[1])
									xid, err := strconv.ParseInt(data[0], 10, 64)
									if err != nil {
										go SendMsg2(uid, "การป้อนข้อมูลไม่ถูกต้อง")
										return
									}
									orsormorData.AddOrsormor(xid, data[1])
									DeleteUserHealth(xid)
									user.DeleteData(xid)
									SendMsg2(uid, "บันทึกข้อมูล อสม. เรียบร้อยแล้ว")

								default:
									SendMsg2(uid, "การป้อนข้อมูลไม่ถูกต้อง")
								}
							}()
						case "orsormordel":
							go func() {
								oper[uid] = ""
								xid, err := strconv.ParseInt(text, 10, 64)
								if err != nil {
									SendMsg2(uid, "ทำการลบ อสม. ไม่สำเร็จ")
									return
								}
								switch orsormorData.IsOrsormor(xid) {
								case true:
									name := orsormorData.GetOrsormorName(xid)
									orsormorData.DeleteOrsormor(xid)
									SendMsg2(uid, fmt.Sprintf("ลบ อสม. %s(x%d) เรียบร้อยแล้ว", name, xid))
								default:
									SendMsg2(uid, "ทำการลบ อสม. ไม่สำเร็จ")
								}
							}()
						default:
							go SendMsgNotUS2(uid)
						}
					}
				}
			}
		}(update)
	}
}

func IsOperators(uid int64) bool {
	for i := range operators {
		if operators[i].ID == uid {
			return true
		}
	}
	return false
}

func GetAppointment(pid int64) []appointData {
	appoint := make([]appointData, 0)
	visitno := GetAllVisitnoFromPid(pid)
	appoint = append(appoint, appointVar.GetAppointDataFromVisitAndPid(visitno, pid)...)
	return appoint
}

func ExtractDateFromInt(d int) (int, int, int) {
	year := d / 10000
	x := d - (year * 10000)
	month := x / 100
	day := x - (month * 100)

	return year, month, day
}

func MakeTextForAppointChange(appType string) string {
	txt := ""
	switch appType {
	case "Diag":
		txt = "_ขอเปลี่ยนวันนัดพบแพทย์เพื่อตรวจ_"
	case "EPI":
		txt = "_ขอเปลี่ยนวันนัดเพื่อรับวัคซีน_"
	case "FP":
		txt = "_ขอเปลี่ยนวันนัดเพื่อรับบริการวางแผนครอบครัว_"
	case "Home":
		txt = "_ขอเปลี่ยนวันนัดเพื่อตรวจเยี่ยมบ้าน_"
	case "Nutri":
		txt = "_ขอเปลี่ยนวันนัดเพื่อตรวจโภชนาการ_"
	case "Baby":
		txt = "_ขอเปลี่ยนวันนัดเพื่อรับบริการการดูแลทารกหลังคลอด_"
	case "Mother":
		txt = "_ขอเปลี่ยนวันนัดเพื่อรับบริการการดูแลหลังคลอดมารดา_"
	}
	return txt
}

func MakeTextForAppointAdvice(appType string) string {
	txt := "*คำแนะนำ : *"
	switch appType {
	case "Diag":
		txt += "_นัดพบแพทย์เพื่อตรวจ_"
	case "EPI":
		txt += "_นัดเพื่อรับวัคซีน_"
	case "FP":
		txt += "_นัดเพื่อรับบริการวางแผนครอบครัว_"
	case "Home":
		txt += "_นัดเพื่อตรวจเยี่ยมบ้าน_"
	case "Nutri":
		txt += "_นัดเพื่อตรวจโภชนาการ_"
	case "Baby":
		txt += "_นัดเพื่อรับบริการการดูแลทารกหลังคลอด_"
	case "Mother":
		txt += "_นัดเพื่อรับบริการการดูแลหลังคลอดมารดา_"
	}
	return txt
}

func chkAppointCase(appType string) bool {
	switch appType {
	case "Diag":
		return true
	case "EPI":
		return true
	case "FP":
		return true
	case "Home":
		return true
	case "Nutri":
		return true
	case "Baby":
		return true
	case "Mother":
		return true
	}
	return false
}

func MakeTimeFromInt(d int) time.Time {
	year, month, day := ExtractDateFromInt(d)
	t, _ := time.Parse("2006-01-02", fmt.Sprintf("%d-%02d-%02d", year, month, day))
	return t
}

func ShowDateText(appoint int) string {
	t := MakeTimeFromInt(appoint)
	dd := t.Weekday()
	year, month, day := t.Date()
	return fmt.Sprintf("%s ที่ %d %s %d", MakeWeekdayText(int(dd)), day, MakeMonthText(int(month)), year+543)
}

func MakeMonthText(m int) string {
	switch m {
	case 1:
		return "มกราคม"
	case 2:
		return "กุมภาพันธ์"
	case 3:
		return "มีนาคม"
	case 4:
		return "เมษายน"
	case 5:
		return "พฤษภาคม"
	case 6:
		return "มิถุนายน"
	case 7:
		return "กรกฎาคม"
	case 8:
		return "สิงหาคม"
	case 9:
		return "กันยายน"
	case 10:
		return "ตุลาคม"
	case 11:
		return "พฤศจิกายน"
	case 12:
		return "ธันวาคม"
	}
	return ""
}

func MakeWeekdayText(w int) string {
	t := "วัน"
	switch w {
	case 0:
		return t + "อาทิตย์"
	case 1:
		return t + "จันทร์"
	case 2:
		return t + "อังคาร"
	case 3:
		return t + "พุธ"
	case 4:
		return t + "พฤหัส"
	case 5:
		return t + "ศุกร์"
	case 6:
		return t + "เสาร์"
	}
	return t
}
func ConvDateToInt(d string) int {
	dateText := strings.Split(d, "-")
	day, err := strconv.Atoi(dateText[2])
	if err != nil {
		return 0
	}
	month, err := strconv.Atoi(dateText[1])
	if err != nil {
		return 0
	}
	year, err := strconv.Atoi(dateText[0])
	if err != nil {
		return 0
	}

	return (year * 10000) + (month * 100) + day
}

func GetToday() int {
	return ConvDateToInt(fmt.Sprintf("%s", time.Now().Format("2006-01-02")))
}

func GetDayPlus(d int) int {
	return ConvDateToInt(fmt.Sprintf("%s", time.Now().Add(time.Duration(d)*time.Duration(24)*time.Hour).Format("2006-01-02")))
}

func GetUserData(uid int64) {
	pid := int64(0)
	fname := ""
	lname := ""

	switch user.GetState(uid, "idcard") {
	case "":
		user.SetState(uid, "idcard", GetIDcardFromPid(user.Get(uid)))
	}

	pid, fname, lname = GetPersonFromIDcard(user.GetState(uid, "idcard"))

	user.SetState(uid, "fname", fname)
	user.SetState(uid, "lname", lname)
	user.Set(uid, pid)

	return
}

func FindPersonNameFromPID(pid int64) (string, string) {
	pidx := user.GetAllPID()
	for k, v := range pidx {
		if pid == v {
			if user.GetState(k, "fname") == "" {
				GetUserData(k)
			}
			return user.GetState(k, "fname"), user.GetState(k, "lname")
		}
	}
	fname, lname := GetPersonNameFromPID(pid)
	return fname, lname
}

func ChkUserBirthDay(uid int64) bool {
	birth := GetBirthDayFromIDcard(user.GetState(uid, "idcard"))
	switch birth {
	case 0:
		return false
	default:
		birthx := ConvDateSlashToInt(user.GetState(uid, "birthday"))
		if birth == birthx {
			return true
		}
		return false
	}
	return false
}

func ConvDateSlashToInt(b string) int {
	bsplit := strings.Split(b, "/")
	if len(bsplit) != 3 {
		return 0
	}
	day, err := strconv.Atoi(bsplit[0])
	if err != nil {
		return 0
	}
	month, err := strconv.Atoi(bsplit[1])
	if err != nil {
		return 0
	}
	year, err := strconv.Atoi(bsplit[2])
	if err != nil {
		return 0
	}

	return ((year - 543) * 10000) + (month * 100) + day
}

func SendMsgNotUS(uid int64) {
	go SendMsg(uid, "ขออภัย ไม่เข้าใจคำสั่ง")
}

func SendMsgNotUS2(uid int64) {
	go SendMsg2(uid, "ขออภัย ไม่เข้าใจคำสั่ง")
}

func SendMsg(user int64, text string) {
	mutexSendBot.Lock()
	defer mutexSendBot.Unlock()

	msg := tg.NewMessage(user, text)
	msg.ParseMode = "Markdown"
	bot.Send(msg)
}

func SendMsg2(user int64, text string) {
	mutexSendBot2.Lock()
	defer mutexSendBot2.Unlock()

	msg := tg.NewMessage(user, text)
	msg.ParseMode = "Markdown"
	bot2.Send(msg)
}

func SendMsgAllOper(text string) {
	for i := range operators {
		go SendMsg2(operators[i].ID, text)
	}
}

func SendMsgPID(uid int64, pid int64, text string) bool {
	apid := user.GetAllPID()
	for k, v := range apid {
		if v == pid {
			operName := GetOperNameFromUid(uid)
			if user.GetState(k, "fname") == "" {
				GetUserData(k)
			}
			dataT := fmt.Sprintf("_เจ้าหน้าที่ (%s)_\n\n%s", operName, text)
			operT := fmt.Sprintf("_เจ้าหน้าที่ (%s) ได้คุยกับ_\n*คุณ %s %s (%d)*\n\n%s", operName, user.GetState(k, "fname"), user.GetState(k, "lname"), v, text)
			go SendMsg(k, dataT)
			go SendMsgAllOper(operT)
			return true
		}
	}
	return false
}

func SendMsgAllOrsormorCare(uid int64, pid int64, text string) bool {
	ouid := orsormorData.FindAllUidFromPid(pid)
	for i := range ouid {
		operName := GetOperNameFromUid(uid)
		dataT := fmt.Sprintf("_เจ้าหน้าที่ (%s)_\n\n%s", operName, text)
		operT := fmt.Sprintf("_เจ้าหน้าที่ (%s) ได้คุยกับ_\n*คุณ %s (x%d)*\n\n%s", operName, orsormorData.GetOrsormorName(ouid[i]), ouid[i], text)
		go SendMsg(ouid[i], dataT)
		go SendMsgAllOper(operT)
		return true
	}
	return false
}

func SendMsgOrsormor(uid int64, pid int64, text string) {
	name := orsormorData.GetOrsormorName(pid)
	operName := GetOperNameFromUid(uid)
	dataT := fmt.Sprintf("_เจ้าหน้าที่ (%s)_\n\n%s", operName, text)
	operT := fmt.Sprintf("_เจ้าหน้าที่ (%s) ได้คุยกับ_\n*คุณ %s (x%d)*\n\n%s", operName, name, pid, text)
	go SendMsg(pid, dataT)
	go SendMsgAllOper(operT)
}

func GetOperNameFromUid(uid int64) string {
	operName := ""
	for i := range operators {
		if operators[i].ID == uid {
			operName = operators[i].Name
			break
		}
	}
	return operName
}

func SendInform(uid int64, text string) {
	apid := user.GetAllPID()
	orsor := orsormorData.ListOrsormor()
	operName := GetOperNameFromUid(uid)
	dataT := fmt.Sprintf("_ประชาสัมพันธ์ (%s)_\n\n%s", operName, text)
	for k, _ := range apid {
		go SendMsg(k, dataT)
	}
	for i := range orsor {
		go SendMsg(orsor[i], dataT)
	}
	go SendMsgAllOper(dataT)
}

func RestoreUser() {
	for k, v := range GetAllUserHealth() {
		user.Set(k, v)
	}
}

func TimerFor_appointVar() {
	for {
		time.Sleep(5 * time.Minute)
		appointVar.Refresh()
	}
}

func StartAlertTimer() {
	ticker := time.NewTicker(time.Minute)
	alertTXT := strings.Split(alertTime, ":")
	if len(alertTXT) != 2 {
		fmt.Printf("Config error in alerttime")
		os.Exit(1)
	}
	hour, err := strconv.ParseInt(alertTXT[0], 10, 0)
	if err != nil {
		fmt.Printf("Config error in alerttime")
		os.Exit(1)
	}
	minute, err := strconv.ParseInt(alertTXT[1], 10, 0)
	if err != nil {
		fmt.Printf("Config error in alerttime")
		os.Exit(1)
	}
	for {
		<-ticker.C
		go func() {
			now := time.Now()
			h := now.Hour()
			m := now.Minute()
			if int(hour) == h && int(minute) == m {
				go AlertAppointment(0, 3)
				go AlertAppointmentOrsormor(0, 3)
			}
		}()
	}
}

func AlertAppointmentOrsormor(uid int64, dayPlus int) {
	xpid, _, _ := orsormorData.GetOrsormorAllCare(uid)
	if dayPlus == 0 {
		switch len(xpid) {
		case 0:
			SendMsg(uid, "ท่านยังไม่มีผู้รับบริการในการดูแล")
		default:
			for i := range xpid {
				go AlertAppointmentOrsormorX(uid, xpid[i])
			}
		}
		return
	}
	appData := appointVar.GetAllAppointData()
	for i := 0; i <= dayPlus; i++ {
		go func(ix int) {
			appDateAlert := GetDayPlus(ix)
			for u := range appData {
				if appData[u].dateApp == appDateAlert {
					pid := int64(0)
					switch appData[u].pid {
					case 0:
						pid = GetPidFromVisitno(appData[u].visitno)
					default:
						pid = appData[u].pid
					}
					for _, v := range xpid {
						if v == pid {
							go AlertAppointmentOrsormorX(uid, pid)
						}
					}
				}
			}
		}(i)
	}
}

func AlertAppointmentOrsormorX(uid int64, pid int64) {
	appoint := GetAppointment(pid)
	fname, lname := orsormorData.GetCareName(pid)
	switch len(appoint) {
	case 0:
		if uid != 0 {
			SendMsg(uid, fmt.Sprintf("*คุณ %s %s (%d)*\nไม่มีนัดพบแพทย์", fname, lname, pid))
		}
	default:
		switch uid {
		case 0:
			uuid := orsormorData.FindAllUidFromPid(pid)
			for ix := range uuid {
				for i := range appoint {
					dtxt := ""
					switch appoint[i].typeApp {
					case "Diag":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดพบแพทย์เพื่อตรวจ_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					case "EPI":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับวัคซีน_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					case "FP":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการวางแผนครอบครัว_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					case "Home":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อตรวจเยี่ยมบ้าน_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					case "Nutri":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อตรวจโภชนาการ_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					case "Baby":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการการดูแลทารกหลังคลอด_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					case "Mother":
						dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการการดูแลหลังคลอดมารดา_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
					}

					adviceData := GetAdviceUser(appoint[i].typeApp)
					switch adviceData {
					case "":
						SendMsg(uuid[ix], dtxt)
					default:
						SendMsg(uuid[ix], fmt.Sprintf("%s\n\n%s\n\n%s", dtxt, MakeTextForAppointAdvice(appoint[i].typeApp), adviceData))
					}
				}
			}
		default:
			for i := range appoint {
				dtxt := ""
				switch appoint[i].typeApp {
				case "Diag":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดพบแพทย์เพื่อตรวจ_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				case "EPI":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับวัคซีน_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				case "FP":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการวางแผนครอบครัว_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				case "Home":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อตรวจเยี่ยมบ้าน_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				case "Nutri":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อตรวจโภชนาการ_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				case "Baby":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการการดูแลทารกหลังคลอด_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				case "Mother":
					dtxt = fmt.Sprintf("*คุณ %s %s (%d)*\n\n_มีนัดเพื่อรับบริการการดูแลหลังคลอดมารดา_\n\nใน %s", fname, lname, pid, ShowDateText(appoint[i].dateApp))
				}

				adviceData := GetAdviceUser(appoint[i].typeApp)
				switch adviceData {
				case "":
					SendMsg(uid, dtxt)
				default:
					SendMsg(uid, fmt.Sprintf("%s\n\n%s\n\n%s", dtxt, MakeTextForAppointAdvice(appoint[i].typeApp), adviceData))
				}
			}
		}
	}

}

func AlertAppointment(uid int64, dayPlus int) {
	if dayPlus == 0 {
		go AlertAppointmentX(uid)
		return
	}
	appData := appointVar.GetAllAppointData()
	uidData := user.GetAllPID()
	for i := 0; i <= dayPlus; i++ {
		go func(ix int) {
			appDateAlert := GetDayPlus(ix)
			for u := range appData {
				if appData[u].dateApp == appDateAlert {
					pid := int64(0)
					switch appData[u].pid {
					case 0:
						pid = GetPidFromVisitno(appData[u].visitno)
					default:
						pid = appData[u].pid
					}
					for k, v := range uidData {
						if v == pid {
							go AlertAppointmentX(k)
						}
					}
				}
			}
		}(i)
	}
}

func AlertAppointmentX(uid int64) {
	appoint := GetAppointment(user.Get(uid))
	switch len(appoint) {
	case 0:
		SendMsg(uid, "ท่านไม่มีนัดพบแพทย์")
	default:
		for i := range appoint {
			dtxt := ""
			switch appoint[i].typeApp {
			case "Diag":
				dtxt = fmt.Sprintf("_ท่านมีนัดพบแพทย์เพื่อตรวจ_\nใน %s", ShowDateText(appoint[i].dateApp))
			case "EPI":
				dtxt = fmt.Sprintf("_ท่านมีนัดเพื่อรับวัคซีน_\nใน %s", ShowDateText(appoint[i].dateApp))
			case "FP":
				dtxt = fmt.Sprintf("_ท่านมีนัดเพื่อรับบริการวางแผนครอบครัว_\nใน %s", ShowDateText(appoint[i].dateApp))
			case "Home":
				dtxt = fmt.Sprintf("_ท่านมีนัดเพื่อตรวจเยี่ยมบ้าน_\nใน %s", ShowDateText(appoint[i].dateApp))
			case "Nutri":
				dtxt = fmt.Sprintf("_ท่านมีนัดเพื่อตรวจโภชนาการ_\nใน %s", ShowDateText(appoint[i].dateApp))
			case "Baby":
				dtxt = fmt.Sprintf("_ท่านมีนัดเพื่อรับบริการการดูแลทารกหลังคลอด_\nใน %s", ShowDateText(appoint[i].dateApp))
			case "Mother":
				dtxt = fmt.Sprintf("_ท่านมีนัดเพื่อรับบริการการดูแลหลังคลอดมารดา_\nใน %s", ShowDateText(appoint[i].dateApp))
			}

			adviceData := GetAdviceUser(appoint[i].typeApp)
			switch adviceData {
			case "":
				SendMsg(uid, dtxt)
			default:
				SendMsg(uid, fmt.Sprintf("%s\n\n%s\n\n%s", dtxt, MakeTextForAppointAdvice(appoint[i].typeApp), adviceData))
			}
		}
	}
}

func LoadOrsormor() {
	orsormorData.uid = LoadOrsormorDB()
	orsormorData.care = LoadOrsormorCareDB()
}

func main() {
	ConfigProcess()
	LoadOrsormor()
	RestoreUser()
	appointVar.Refresh()
	go TimerFor_appointVar()
	go StartBOT()
	go StartBOT2()
	go StartAlertTimer()
	select {}
}
