package main

import (
	"sync"
)

type (
	orsormor struct {
		sync.Mutex
		uid       map[int64]string
		care      map[int64][]int64
		careFName map[int64]string
		careLName map[int64]string
		state     map[int64]map[string]string
	}
)

func (r *orsormor) SetState(uid int64, state string, data string) {
	r.Lock()
	defer r.Unlock()

	switch r.state[uid] {
	case nil:
		r.state[uid] = make(map[string]string)
	}
	r.state[uid][state] = data
}

func (r *orsormor) GetState(uid int64, state string) string {
	r.Lock()
	defer r.Unlock()

	switch r.state[uid] {
	case nil:
		r.state[uid] = make(map[string]string)
	}
	return r.state[uid][state]
}

func (r *orsormor) GetCareName(pid int64) (string, string) {
	r.Lock()
	defer r.Unlock()

	switch r.careFName[pid] {
	case "":
		fnamex, lnamex := GetPersonNameFromPID(pid)
		r.careFName[pid] = fnamex
		r.careLName[pid] = lnamex
	}

	return r.careFName[pid], r.careLName[pid]
}

func (r *orsormor) SetCareName(pid int64, fname string, lname string) {
	r.Lock()
	defer r.Unlock()

	r.careFName[pid] = fname
	r.careLName[pid] = lname
}

func (r *orsormor) IsOrsormor(uid int64) bool {
	r.Lock()
	defer r.Unlock()

	for k, _ := range r.uid {
		if uid == k {
			return true
		}
	}
	return false
}

func (r *orsormor) ListOrsormor() []int64 {
	r.Lock()
	defer r.Unlock()

	uid := make([]int64, 0)

	for k, _ := range r.uid {
		uid = append(uid, k)
	}
	return uid
}

func (r *orsormor) AddOrsormor(uid int64, name string) {
	r.Lock()
	defer r.Unlock()

	r.uid[uid] = name
	SaveOrsormorDB(uid, name)
}

func (r *orsormor) DeleteOrsormor(uid int64) {
	r.Lock()
	defer r.Unlock()

	delete(r.uid, uid)
	delete(r.care, uid)
	DeleteOrsormorDB(uid)
}

func (r *orsormor) GetOrsormorAllName() map[int64]string {
	r.Lock()
	defer r.Unlock()

	orsor := make(map[int64]string)

	for k, v := range r.uid {
		orsor[k] = v
	}

	return orsor
}

func (r *orsormor) GetOrsormorName(uid int64) string {
	r.Lock()
	defer r.Unlock()

	return r.uid[uid]
}

func (r *orsormor) AddOrsormorCare(uid int64, pid int64) {
	r.Lock()
	defer r.Unlock()

	for i := range r.care[uid] {
		if r.care[uid][i] == pid {
			return
		}
	}
	r.care[uid] = append(r.care[uid], pid)
	AddOrsormorCareDB(uid, pid)
}

func (r *orsormor) GetOrsormorAllCare(uid int64) ([]int64, []string, []string) {
	r.Lock()
	defer r.Unlock()
	pidx := make(map[int64]bool)

	pid := make([]int64, 0)
	fname := make([]string, 0)
	lname := make([]string, 0)

	switch uid {
	case 0:
		for i := range r.care {
			for ii := range r.care[i] {
				pidx[r.care[i][ii]] = true
			}
		}
		for i := range pidx {
			pid = append(pid, i)
			switch r.careFName[i] {
			case "":
				fnamex, lnamex := GetPersonNameFromPID(i)
				r.careFName[i] = fnamex
				r.careLName[i] = lnamex
			}
			fname = append(fname, r.careFName[i])
			lname = append(lname, r.careLName[i])
		}
	default:
		for i := range r.care[uid] {
			pid = append(pid, r.care[uid][i])
			switch r.careFName[r.care[uid][i]] {
			case "":
				fnamex, lnamex := GetPersonNameFromPID(r.care[uid][i])
				r.careFName[r.care[uid][i]] = fnamex
				r.careLName[r.care[uid][i]] = lnamex
			}
			fname = append(fname, r.careFName[r.care[uid][i]])
			lname = append(lname, r.careLName[r.care[uid][i]])
		}
	}
	return pid, fname, lname
}

func (r *orsormor) RemoveCareFromOrsormor(uid int64, pid int64) bool {
	r.Lock()
	defer r.Unlock()

	success := false
	carePID := make([]int64, 0)
	for i := range r.care[uid] {
		if r.care[uid][i] == pid {
			success = true
			continue
		}
		carePID = append(carePID, r.care[uid][i])
	}
	if success {
		r.care[uid] = carePID
		RemoveCareFromOrsormorDB(uid, pid)
	}
	return success
}

func (r *orsormor) FindAllUidFromPid(pid int64) []int64 {
	uid := make([]int64, 0)
	for i := range r.care {
		for ii := range r.care[i] {
			if r.care[i][ii] == pid {
				uid = append(uid, i)
				break
			}
		}
	}
	return uid
}
