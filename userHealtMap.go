package main

import "sync"

type (
	userHealthMap struct {
		sync.Mutex
		pid   map[int64]int64
		state map[int64]map[string]string
	}
)

func (r *userHealthMap) GetAllPID() map[int64]int64 {
	r.Lock()
	defer r.Unlock()

	pid := make(map[int64]int64)
	for k, v := range r.pid {
		pid[k] = v
	}
	return pid
}

func (r *userHealthMap) Get(uid int64) int64 {
	r.Lock()
	defer r.Unlock()

	return r.pid[uid]
}

func (r *userHealthMap) Set(uid int64, pid int64) {
	r.Lock()
	defer r.Unlock()

	r.pid[uid] = pid
}

func (r *userHealthMap) DeleteData(uid int64) {
	r.Lock()
	defer r.Unlock()

	delete(r.pid, uid)
	delete(r.state, uid)
}

func (r *userHealthMap) GetState(uid int64, state string) string {
	r.Lock()
	defer r.Unlock()

	switch r.state[uid] {
	case nil:
		r.state[uid] = make(map[string]string)
	}
	return r.state[uid][state]
}

func (r *userHealthMap) SetState(uid int64, state string, data string) {
	r.Lock()
	defer r.Unlock()

	switch r.state[uid] {
	case nil:
		r.state[uid] = make(map[string]string)
	}
	r.state[uid][state] = data
}
