package main

import (
	"sync"
)

type (
	appointData struct {
		dateApp int
		visitno int64
		pid     int64
		typeApp string
	}
	appointMatrix struct {
		sync.Mutex
		appoint []appointData
	}
)

func (a *appointMatrix) GetLen() int {
	a.Lock()
	defer a.Unlock()

	return len(a.appoint)
}

func (a *appointMatrix) GetPid(i int) int64 {
	a.Lock()
	defer a.Unlock()

	return a.appoint[i].pid
}

func (a *appointMatrix) GetVisitno(i int) int64 {
	a.Lock()
	defer a.Unlock()

	return a.appoint[i].visitno
}

func (a *appointMatrix) GetDateApp(i int) int {
	a.Lock()
	defer a.Unlock()

	return a.appoint[i].dateApp
}
func (a *appointMatrix) GetTypeApp(i int) string {
	a.Lock()
	defer a.Unlock()

	return a.appoint[i].typeApp
}

func (a *appointMatrix) Refresh() {
	data := make([]appointData, 0)
	today := GetToday()

	diag := GetAllAppointFromDateVisitnoinTable(today, "appodate", "visitdiagappoint", "Diag")
	data = append(data, diag...)

	epi := GetAllAppointFromDatePIDinTable(today, "dateappoint", "visitepiappoint", "EPI")
	data = append(data, epi...)

	fp := GetAllAppointFromDatePIDinTable(today, "datedue", "visitfp", "FP")
	data = append(data, fp...)

	homehealth := GetAllAppointFromDateVisitnoinTable(today, "dateappoint", "visithomehealthindividual", "Home")
	data = append(data, homehealth...)

	nutri := GetAllAppointFromDateVisitnoinTable(today, "dateappoint", "visitnutrition", "Nutri")
	data = append(data, nutri...)

	baby := GetAllAppointFromDatePIDinTable(today, "dateappointcare", "visitbabycare", "Baby")
	data = append(data, baby...)

	mother := GetAllAppointFromDatePIDinTable(today, "dateappointcare", "visitancmothercare", "Mother")
	data = append(data, mother...)

	a.Lock()
	defer a.Unlock()
	a.appoint = data
}

func (a *appointMatrix) GetAppointDataFromVisitAndPid(visitno []int64, pid int64) []appointData {
	a.Lock()
	defer a.Unlock()

	data := make([]appointData, 0)
	for i := range a.appoint {
		if pid == a.appoint[i].pid {
			data = append(data, a.appoint[i])
			continue
		}
		for ii := range visitno {
			if visitno[ii] == a.appoint[i].visitno {
				data = append(data, a.appoint[i])
				continue
			}
		}
	}
	return data
}

func (a *appointMatrix) GetAllAppointData() []appointData {
	a.Lock()
	defer a.Unlock()

	appData := make([]appointData, 0)
	appData = append(appData, a.appoint...)
	return appData
}
